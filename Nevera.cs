﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Tasca_2_Threads
{
    class Nevera
    {
        int beers;
        int MaxBeers;
        Random rnd = new Random();
        public Nevera(int startBeers)
        {
            MaxBeers = 9;
            beers = startBeers <= MaxBeers ? startBeers : MaxBeers;
        }
        public void FillBeers(string name){
            int fillValue = rnd.Next(1, 7);
            
            beers = Math.Min(fillValue+beers, MaxBeers);
            Console.WriteLine($"{name} ha afegit {fillValue} cerveses");
            Console.WriteLine($"Hi han {beers} cerveses");
            if (beers == MaxBeers)
                Console.WriteLine($"La nevera és plena!");
        }
        public void ConsumeBeers(string name)
        {
            int consumeValue = rnd.Next(1, 7);

            beers = Math.Max(beers - consumeValue, 0);
            Console.WriteLine($"{name} ha begut {consumeValue} cerveses");       
            if (beers == 0)
                Console.WriteLine($"La nevera és buida!");
            else
                Console.WriteLine($"Hi han {beers} cerveses");
        }

          

    }
}
