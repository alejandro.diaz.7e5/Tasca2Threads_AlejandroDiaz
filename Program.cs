﻿using System;
using System.Threading;

namespace Tasca_2_Threads
{
    class Program
    {

        enum actions { fill, consume };
        static void Main(string[] args)
        {

            Console.WriteLine("Hello World!");
            Exercise1();
            Exercise2();
        }

        private static void Exercise1()
        {
            Thread t1 = new Thread(() => WriteALine("Hi havia una vegada", null));
            Thread t2 = new Thread(() => WriteALine("En un lugar de la mancha", t1));
            Thread t3 = new Thread(() => WriteALine("Once upon a time", t2));
            t1.Start();
            t2.Start();
            t3.Start();
            t3.Join();
        }
        private static void Exercise2()
        { 

            Nevera nevera = new Nevera(6);
            Thread x1 = new Thread(() => AccessNevera("Annita", actions.fill, nevera, null));
            Thread x2 = new Thread(() => AccessNevera("Bad Bunny", actions.consume, nevera, x1));
            Thread x3 = new Thread(() => AccessNevera("Lil Nas X", actions.consume,nevera, x2));
            Thread x4 = new Thread(() => AccessNevera("Manuel Turizo",actions.consume,nevera, x3));
            x1.Start();
            x2.Start();
            x3.Start();
            x4.Start();

        }

          

        static void WriteALine(string text, Thread? threadToJoin)
        {
            if (threadToJoin != null)
                threadToJoin.Join();
            string[] words = text.Split();
            foreach (string word in words) 
            {
                Console.Write(word+ " ");
                Thread.Sleep(500); 
            }
            Console.WriteLine();
        }
        static void AccessNevera(string name, actions action, Nevera nevera, Thread? threadToJoin)
        {
            if (threadToJoin != null)
                threadToJoin.Join();
            switch (action)
            {
                case actions.fill:
                    nevera.FillBeers(name);
                    break;
                case actions.consume:
                    nevera.ConsumeBeers(name);
                    break;
            }
            Thread.Sleep(500);
        } 
    }
}